# PagaDirect Plugin for WooCommerce #

Allow payments with PagaDirect on your WooCommerce website.

License: MIT

### Version Information ###

* Requires at least: 4.0
* Tested up to: 5.4.1
* Requires PHP: 5.5

### How to install? ###

##### Uploading in WordPress Dashboard #####

1. Download this repository as ZIP file.
2. Navigate to the 'Add New' in the plugins dashboard
3. Navigate to the 'Upload' area
4. Select `pagadirect-woocommerce-plugin.zip` from your computer
5. Click 'Install Now'
6. Activate the plugin in the Plugin dashboard

##### Using FTP ##### 

1. Download this repository as ZIP file.
2. Extract the zip file on your computer.
3. Upload the `pagadirect-woocommerce-plugin` directory to the `/wp-content/plugins/` directory
4. Activate the plugin in the Plugin dashboard

##### Using The WordPress Dashboard #####

1. Navigate to the 'Add New' in the plugins dashboard
2. Search for 'PagaDirect'
3. Click 'Install Now'
4. Activate the plugin on the Plugin dashboard

### Changelog ###

##### 1.0.0 #####
* Initial release