<?php
/*
  Plugin Name: PagaDirect
  Description: PagaDirect Payment Integration.
  Version: 1.0.0
 */

if (!defined('ABSPATH')) {
    exit; // Exit if accessed directly
}

define('PAGADIRECT_PLUGIN_URL', plugin_dir_url(__FILE__));
define('PAGADIRECT_PLUGIN_PATH', plugin_dir_path(__FILE__));

/**
 * Initiate PagaDirect once plugin is ready
 */
add_action('plugins_loaded', 'woocommerce_pagadirect_init');

function woocommerce_pagadirect_init() {

    class WC_PagaDirect extends WC_Payment_Gateway {

        public $domain;

        /**
         * Constructor for the gateway.
         */
        public function __construct() {
            $this->domain = 'pagadirect';

            $this->id = 'pagadirect';
            $this->icon = PAGADIRECT_PLUGIN_URL . 'assets/images/logo.png';
            $this->has_fields = false;
            $this->method_title = __('PagaDirect', $this->domain);
            $this->method_description = __('PagaDirect Payment Integration.', $this->domain);
            
            // Define user set variables
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            $this->api_key = $this->get_option('api_key');
            $this->test_mode = $this->get_option('test_mode');

	    // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Actions
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array($this, 'process_admin_options'));
            add_action('woocommerce_thankyou_' . $this->id, array($this, 'thankyou_page'));
            add_action('woocommerce_api_'. strtolower("WC_PagaDirect"), array( $this, 'check_ipn_response' ) );
            
        }

        /**
         * Initialize Gateway Settings Form Fields.
         */
        public function init_form_fields() {
    
	    $field_arr = array(
                'enabled' => array(
                    'title' => __('Enable PagaDirect Plugin?', $this->domain),
                    'type' => 'checkbox',
                    'label' => __(' ', $this->domain),
                    'default' => 'yes'
                ),
                'title' => array(
                    'title' => __('Title', $this->domain),
                    'type' => 'text',
                    'description' => __('The title which the user sees during checkout.', $this->domain),
                    'default' => $this->method_title,
                    'desc_tip' => true,
                ),
                'description' => array(
                    'title' => __('Description', $this->domain),
                    'type' => 'textarea',
                    'description' => __('Description that the customer will see on your checkout.', $this->domain),
                    'desc_tip' => true,
                    'default' => __('Pay with iDeal, Bancontact, Credit Cards,...', $this->domain),
                ),
                'api_key' => array(
                    'title' => __('API Key', $this->domain),
                    'type' => 'text',
                    'description' => __('Please contact PagaDirect for your API Key.', $this->domain),
                    'default' => '',
                    'desc_tip' => true,
                ),
                'test_mode' => array(
                    'title' => __('Enable Test Mode?', $this->domain),
                    'type' => 'checkbox',
                    'label' => __(' ', $this->domain),
                    'default' => 'yes'
                ),
            );

            $this->form_fields = $field_arr;
        }

        /**
         * Process Gateway Settings Form Fields.
         */
	public function process_admin_options() {
            $this->init_settings();

            $post_data = $this->get_post_data();
            if (empty($post_data['woocommerce_pagadirect_api_key'])) {
                WC_Admin_Settings::add_error(__('Please enter PagaDirect API Key', $this->domain));
            } else {
                foreach ( $this->get_form_fields() as $key => $field ) {
                    $setting_value = $this->get_field_value( $key, $field, $post_data );
                    $this->settings[ $key ] = $setting_value;
                }
                return update_option( $this->get_option_key(), apply_filters( 'woocommerce_settings_api_sanitized_fields_' . $this->id, $this->settings ) );
            }
	}

        /**
         * Output for the order received page.
         */
        public function thankyou_page($order_id) {
            global $woocommerce;
            $order = new WC_Order($order_id);
            $status = $order->get_status();
            if ($status == 'pending') {
                $style = "width: 100%;  margin-bottom: 1rem; background: #00ccb8; padding: 20px; color: #fff; font-size: 22px;";
            ?>
                <div class="payment-panel">
                    <div style="<?php echo $style?>">
                    <?php echo __('Your payment is pending.', $this->domain) ?>
                    </div>
                </div>
            <?php
            } else if ($status == 'completed') {
                $style = "width: 100%;  margin-bottom: 1rem; background: green; padding: 20px; color: #fff; font-size: 22px;";
                $woocommerce->cart->empty_cart();
            ?>
                <div class="payment-panel">
                    <div style="<?php echo $style?>">
                    <?php echo __('Your payment is successful', $this->domain) ?>
                    </div>
                </div>
            <?php    
            } else if ($status == 'failed') {
                $style = "width: 100%;  margin-bottom: 1rem; background: indianred; padding: 20px; color: #fff; font-size: 22px;";
                $error_message = '';
                if (isset($_GET['pagaerr'])) {
                    $error_message = base64_decode($_GET['pagaerr']);
                }
            ?>
                 <div class="payment-panel">
                    <div style="<?php echo $style?>">
                    <?php echo __('Your payment has failed', $this->domain) ?>
                    <?php if (!empty($error_message)) {?>
                        <?php echo $error_message ?>
                    <?php }?>
                    </div>
                </div>
            <?php
            } else if ($status == 'cancelled') {
                $style = "width: 100%;  margin-bottom: 1rem; background: indianred; padding: 20px; color: #fff; font-size: 22px;";
            ?>
                 <div class="payment-panel">
                    <div style="<?php echo $style?>">
                    <?php echo __('Your order is cancelled. ', $this->domain) ?>
                    </div>
                </div>
            <?php
            }
        }
        
        public function check_ipn_response() {
            global $woocommerce;
            
            $order_id = base64_decode($_REQUEST['key']);
            $order = new WC_Order($order_id);
            
            $header = array(
                "Content-Type: application/json",
                "PagaDirect-Api-Key: ".$this->api_key
            );

            $transaction_id = $order->get_meta('pagadirect_transaction_id');

            $options = array(
                CURLOPT_URL => $this->getGatewayUrl().'payments/'.$transaction_id, 
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_HEADER => false, 
                CURLOPT_SSL_VERIFYPEER => false, 
                CURLOPT_POST => false, 
                CURLOPT_HTTPHEADER => $header
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
   
            $error_message = '';
            if (!$response) {
                $error_message = curl_error($ch);
                curl_close($ch);
            } else {
                curl_close($ch);
                $result = json_decode($response, true);
                $this->log($result);
                if ($result['success']) {
                    $order->delete_meta_data('pagadirect_transaction_id');
                    $order->delete_meta_data('pagadirect_amount');
                    $order->delete_meta_data('pagadirect_currency');
                    
                    $order->add_meta_data('pagadirect_transaction_id', $result['transaction_id']);
                    $order->add_meta_data('pagadirect_amount', $result['amount']);
                    $order->add_meta_data('pagadirect_currency', $result['currency']);
                    $order->add_meta_data('pagadirect_payment_status', $result['status']);
                    $order->save_meta_data();
                    if ($result['status'] == 'paid') {
                        $order->update_status('completed', __('Payment successful! Transaction Id: '.$result['transaction_id'], $this->domain));
                    } else {
                        $order->update_status('failed', __('Payment Failed. ', $this->domain));
                    }
                    wp_redirect($this->get_return_url($order));
                } else {
                    $error_message = $result['error'];
                }
            }
            
            if ($error_message) {
                wc_add_notice(__('Payment error: ', 'woocommerce') . $error_message);
                $order->update_status('failed', __('Payment Failed: '.$error_message, $this->domain));
                wp_redirect($this->get_return_url($order).'&pagaerr='.$error_message);
            }
            exit;
        }

        /**
         * Process the payment and return the result.
         *
         * @param int $order_id
         * @return array
         */
        public function process_payment($order_id) {
            global $woocommerce;
            $order = wc_get_order($order_id);

            $header = array(
                "Content-Type: application/json",
                "PagaDirect-Api-Key: ".$this->api_key
            );

            $params = array(
                "amount" => $order->get_total(),
                "reference" => $order->get_id(),
                "return_url" => site_url().'/?wc-api=wc_pagadirect&key='. base64_encode($order->get_id())
            );

            $options = array(
                CURLOPT_URL => $this->getGatewayUrl().'payments', 
                CURLOPT_RETURNTRANSFER => true, 
                CURLOPT_HEADER => false, 
                CURLOPT_SSL_VERIFYPEER => false, 
                CURLOPT_POST => true, 
                CURLOPT_POSTFIELDS => json_encode($params), 
                CURLOPT_HTTPHEADER => $header
            );

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            $response = curl_exec($ch);
            
            $error_message = '';
            if (!$response) {
                $error_message = curl_error($ch);
                curl_close($ch);
            } else {
                curl_close($ch);
                $result = json_decode($response, true);
                
                if ($result['success']) {
                    $order->delete_meta_data('pagadirect_transaction_id');
                    $order->add_meta_data('pagadirect_transaction_id', $result['transaction_id']);
                    $order->save_meta_data();
                    return array(
                        'result' => 'success',
                        'redirect' => $result['redirect_url']
                    );
                } else {
                    $error_message = $result['error'];
                }
            }
            
            if ($error_message) {
                wc_add_notice(__('Payment error: ', 'woocommerce') . $error_message, 'error');
            }
            return null;
        }
        
        public function getGatewayUrl()
        {
            $url = 'https://api.pagadirect.com/api/';
            if ($this->test_mode == 'yes') {
                $url = 'https://test-api.pagadirect.com/api/';
            }
            return $url;
        }
        
        public function log($content) {
            $debug = true;
            if ($debug == true) {
                $file = PAGADIRECT_PLUGIN_PATH.'debug.log';
                $fp = fopen($file, 'a+');
                fwrite($fp, "\n");
                fwrite($fp, date("Y-m-d H:i:s").": ");
                fwrite($fp, print_r($content, true));
                fclose($fp);
            }
        }
    }
}

add_filter('woocommerce_payment_gateways', 'add_pagadirect_gateway_class');
function add_pagadirect_gateway_class($methods) {
    $methods[] = 'WC_PagaDirect';
    return $methods;
}

add_filter( 'woocommerce_available_payment_gateways', 'enable_pagadirect_gateway' );
function enable_pagadirect_gateway( $available_gateways ) {
    if ( is_admin() ) return $available_gateways;

    if ( isset( $available_gateways['pagadirect'] )) {
        $settings = get_option('woocommerce_pagadirect_settings');
        
        if(empty($settings['api_key'])) {
            unset( $available_gateways['pagadirect'] );
        }
    } 
    return $available_gateways;
}
